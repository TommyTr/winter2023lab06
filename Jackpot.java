public class Jackpot{
	
	public static void main(String[] args){
		
		System.out.println("Welcome!");
		
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while(gameOver != true){
			System.out.println(board);
			if(board.playATurn()){
				gameOver = true;
			}else{
				numOfTilesClosed++;
			}
			
			System.out.println();
		}
		
		if(numOfTilesClosed >= 7){
			System.out.println("You won!");
		}else{
			System.out.println("You lost!");
		}
		
		
	}
}